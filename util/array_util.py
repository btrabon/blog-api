def index_of(array, search_item):
    current_index = 0
    for item in enumerate(array):
        if item == search_item:
            return current_index
        else:
            current_index += 1
    
    return -1