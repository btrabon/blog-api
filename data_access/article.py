from sqlalchemy.sql import update, and_
from datetime import datetime
from data_access.data_access_base import DataAccessBase
from data_models.models_raw import Article
from exceptions.http_exceptions import HttpNotFoundException, HttpForbiddenException

class ArticleDataAccess(DataAccessBase):
    def __init__(self):
        super().__init__()
    
    def get_article(self, article_id=None, is_admin=None, user_article_ids=None):
        if article_id is not None:
            self.object_exists_or_404(Article.article_id == article_id, Article, article_id)

        q = self.default_query(Article)

        if article_id is not None:
            q = q.filter(Article.article_id == article_id)

        if is_admin is False:
            q = q.filter(Article.date_to_publish <= datetime.now())
        
        q = q.filter(Article.is_deleted == False)
        
        if article_id is not None:
            # if we are requesting an individual article then run some validation checks
            # this will make sure that a user cannot bypass validation on the front-end
            result = q.one_or_none()
            # if the article has a price then make sure the current user has paid for the article
            if result.price is not None and result.price > 0 and is_admin is False:
                # if the current user has no paid articles then instantly reject the user
                if user_article_ids is None or len(user_article_ids) == 0:
                    raise HttpForbiddenException(message='User does not have rights to view this article')
                
                # if the current user has paid articles then make sure they have access to this article
                if str(result.article_id) not in user_article_ids:
                    raise HttpForbiddenException(message='User does not have rights to view this article')
                
        else:
            q = q.order_by(Article.date_to_publish.desc())
            result = q.all()
        
        if result is None:
            raise HttpNotFoundException(message='Article does not exist')

        return result
    
    def get_article_by_ids(self, article_ids):
        q = self.default_query(Article)
        q = q.filter(Article.article_id.in_(article_ids))
        q = q.filter(Article.is_deleted == False)

        result = q.all()

        if result is None:
            raise HttpNotFoundException(message='Article ids do not exist')

        return result
    
    def get_last_published_article(self):
        q = self.default_query(Article)
        q = q.filter(Article.is_deleted == False)
        q = q.filter(Article.date_to_publish <= datetime.now())
        q = q.filter((Article.price.is_(None)) | (Article.price <= 0)) # get the last published free article
        q = q.order_by(Article.date_to_publish.desc())

        result = q.first()
        return result
    
    def create_article(self, article_payload):
        article = Article(
            app_user_id = article_payload.get('app_user_id'),
            article_group_id = article_payload.get('article_group_id'),
            title = article_payload.get('title'),
            content = article_payload.get('content'),
            date_to_publish = article_payload.get('date_to_publish'),
            promoted_start_date = article_payload.get('promoted_start_date'),
            promoted_end_date = article_payload.get('promoted_end_date'),
            comments_enabled = article_payload.get('comments_enabled'),
            price = article_payload.get('price'),
            created_date = datetime.utcnow(),
            updated_date = datetime.utcnow()
        )

        self.session.add(article)
        self.session_commit_with_rollback()
        return article
    
    def edit_article(self, article_id, article_payload):
        self.object_exists_or_404(Article.article_id == article_id, Article, article_id)

        article = self.get_article(article_id)
        article.article_group_id = article_payload.get('article_group_id')
        article.title = article_payload.get('title')
        article.content = article_payload.get('content')
        article.date_to_publish = article_payload.get('date_to_publish')
        article.promoted_start_date = article_payload.get('promoted_start_date')
        article.promoted_end_date = article_payload.get('promoted_end_date')
        article.comments_enabled = article_payload.get('comments_enabled')
        article.price = article_payload.get('price')
        article.updated_date = datetime.utcnow()

        self.session.add(article)
        self.session_commit_with_rollback()
        return article
    
    def delete_article(self, article_id):
        self.object_exists_or_404(Article.article_id == article_id, Article, article_id)

        q = update(Article.__table__)
        q = q.where(and_(Article.article_id == article_id, Article.is_deleted == False))
        q = q.values(is_deleted = True)

        article_result = self.session.execute(q)
        self.session_commit_with_rollback()
        return article_result.rowcount == 1