from sqlalchemy.sql import update, and_
from data_access.data_access_base import DataAccessBase
from data_models.models_raw import AppUserAppClaim, AppClaim
from exceptions.http_exceptions import HttpNotFoundException

class AppUserAppClaimDataAccess(DataAccessBase):
    def __init__(self):
        super().__init__()

    def get_app_user_claim(self, app_user_id, claim_name):
        q = (
            self.default_query(AppUserAppClaim)
            .join(AppClaim)
            .filter(AppUserAppClaim.app_claim_id == AppClaim.app_claim_id)
            .filter(AppClaim.claim_name == claim_name)
            .filter(AppUserAppClaim.app_user_id == app_user_id)
        )

        return self.get_single_object_or_404(q, AppUserAppClaim, claim_name)
    
    def update_app_user_claim(self, app_user_id, claim_name, claim_value):
        app_user_app_claim = self.get_app_user_claim(app_user_id, claim_name)

        app_user_app_claim.claim_value = claim_value

        self.session.add(app_user_app_claim)
        self.session_commit_with_rollback()
        return app_user_app_claim
        