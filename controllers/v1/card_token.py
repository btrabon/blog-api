from flask import Response
from flask_restful import abort, reqparse, request
from flask_restful_swagger_2 import swagger, Resource
import stripe

from data_access.app_user_app_claim import AppUserAppClaimDataAccess
from settings import PAYMENT_PROCESSOR_KEY
from util.array_util import index_of
from decorators.general import rest_method
from decorators.security import requires_authentication, get_claim_data

class CardTokenController(Resource):
    @requires_authentication
    @get_claim_data('RESTRICTED_ARTICLE_ACCESS')
    @rest_method
    def post(self, decorator_data=None):
        token = request.get_json()
        stripe.api_key = PAYMENT_PROCESSOR_KEY

        amount = int(token['amount'] * 100)
        charge = stripe.Charge.create(
            amount=amount,
            currency='aud',
            description='Article Purchase',
            source=token['tokenId']
        )

        if charge['status'] == 'succeeded':
            purchased_article_ids = token['articleIds']
            temp_article_ids = decorator_data['claim_data'].split(',') if 'claim_data' in decorator_data and decorator_data['claim_data'] is not None and decorator_data['claim_data'] is not '' else None
            if temp_article_ids is not None and len(temp_article_ids) > 0:
                current_article_ids = [int(article_id) for article_id in temp_article_ids]
            else:
                current_article_ids = None

            app_user_id = decorator_data['app_user_id']

            combined_articles = self._combine_current_purchased_articles(current_article_ids, purchased_article_ids)
            article_list = ','.join(str(article) for article in combined_articles)
            AppUserAppClaimDataAccess().update_app_user_claim(app_user_id, 'RESTRICTED_ARTICLE_ACCESS', article_list)
            return 200, 'Payment Succeeded'
        else:
            return 500, 'Payment Failed'
    
    def _combine_current_purchased_articles(self, current_articles, purchased_articles):
        if (current_articles is not None and len(current_articles) > 0):
            for index, item in enumerate(purchased_articles):
                if (index_of(current_articles, item) == -1):
                    current_articles.append(item)
            
            return current_articles
        else:
            return purchased_articles