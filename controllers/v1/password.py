from flask import Response
from flask_restful import abort, reqparse, request
from flask_restful_swagger_2 import swagger, Resource

from data_access.app_user import AppUserDataAccess
from security.authentication import Authentication
from decorators.general import rest_method
from decorators.security import requires_authentication

class PasswordController(Resource):
    @requires_authentication
    @rest_method
    def post(self, decorator_data):
        password_data = request.get_json()
        password = password_data.get('password')
        app_user_id = decorator_data['app_user_id']

        user_data_access = AppUserDataAccess()
        user = user_data_access.get_user_by_id(app_user_id)

        passwords_equal = Authentication().compare_passwords(password, user.password_hash)
        return {
            'passwords_equal': passwords_equal
        }
