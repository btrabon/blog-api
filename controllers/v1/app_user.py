from flask import Response
from flask_restful import abort, reqparse, request
from flask_restful_swagger_2 import swagger, Resource

from data_access.app_user import AppUserDataAccess
from view_models.app_user import AppUserModel
from security.authentication import Authentication
from decorators.general import rest_method
from decorators.security import requires_authentication

class AppUserController(Resource):
    @requires_authentication
    @rest_method
    def get(self, decorator_data):
        app_user_id = decorator_data['app_user_id']
        data_access = AppUserDataAccess()
        app_user = data_access.get_user_by_id(app_user_id)
        output_user = AppUserModel._construct_for_output(app_user)
        data_access.close_session()
        return output_user

    @rest_method
    def post(self):
        app_user_data = request.get_json()

        # Add validation here

        # get the password and remove it from the object at the same time
        password = app_user_data.pop('password', None)
        hashed_password = Authentication().hash_password(password)
        app_user = AppUserModel(**app_user_data)
        # Only leave the admin user on when one needs to be created
        result = AppUserDataAccess().create_user(app_user, hashed_password)
        #result = AppUserDataAccess().create_admin_user(app_user, hashed_password)
        return AppUserModel._construct(result)
    
    @requires_authentication
    @rest_method
    def put(self, decorator_data):
        app_user_id = decorator_data['app_user_id']
        app_user_data = request.get_json()
        hashed_password = None

        if 'newPassword' in app_user_data:
            password = app_user_data.get('newPassword')
            hashed_password = Authentication().hash_password(password)
        
        data_access = AppUserDataAccess()
        result = data_access.edit_user(app_user_id, app_user_data, hashed_password)
        output_user = AppUserModel._construct_for_output(result)
        data_access.close_session()
        return output_user
