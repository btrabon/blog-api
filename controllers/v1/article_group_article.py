from flask import Response
from flask_restful import abort, reqparse, request
from flask_restful_swagger_2 import swagger, Resource

from data_access.article_group import ArticleGroupDataAccess
from view_models.article import ArticleModel
from decorators.general import rest_method
from decorators.security import optional_authentication, get_claim_data

class ArticleGroupArticleController(Resource):
    @optional_authentication
    @get_claim_data('RESTRICTED_ARTICLE_ACCESS')
    @rest_method
    def get(self, article_group_id, decorator_data=None):
        user_article_ids = decorator_data['claim_data'].split(',') if decorator_data is not None and 'claim_data' in decorator_data and decorator_data['claim_data'] is not None else None

        article_list = ArticleGroupDataAccess().get_articles_by_article_group(article_group_id)
        output_article_list = [ArticleModel._construct_for_output(n, n.app_user, n.article_group, user_article_ids) for n in article_list]
        ArticleGroupDataAccess().close_session()
        return output_article_list