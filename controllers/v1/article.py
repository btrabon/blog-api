from flask import Response
from flask_restful import abort, reqparse, request
from flask_restful_swagger_2 import swagger, Resource

from data_access.article import ArticleDataAccess
from data_access.app_user import AppUserDataAccess
from data_access.article_group import ArticleGroupDataAccess
from view_models.article import ArticleModel
from decorators.general import rest_method
from decorators.security import requires_authentication, optional_authentication, requires_role, get_roles, get_claim_data

class ArticleController(Resource):
    @optional_authentication
    @get_roles
    @get_claim_data('RESTRICTED_ARTICLE_ACCESS')
    @rest_method
    def get(self, article_id=None, decorator_data=None):
        is_admin = 'ADMIN' in decorator_data['user_roles'] if decorator_data is not None and 'user_roles' in decorator_data else False
        is_home_page = bool(request.args['is_home_page']) if 'is_home_page' in request.args else False
        user_article_ids = decorator_data['claim_data'].split(',') if decorator_data is not None and 'claim_data' in decorator_data and decorator_data['claim_data'] is not None else None
        article_ids = request.args['article_ids'].split(',') if 'article_ids' in request.args else None
        
        if article_id is None:
            if article_ids is not None and len(article_ids) > 0:
                article_list = ArticleDataAccess().get_article_by_ids(article_ids)
                output_articles = [ArticleModel._construct_for_output(article, article.app_user, article.article_group) for article in article_list]
                ArticleDataAccess().close_session()
                return output_articles
            elif is_home_page is False:
                article_list = ArticleDataAccess().get_article(is_admin=is_admin)
                output_articles = [ArticleModel._construct_for_output(article, article.app_user, article.article_group, user_article_ids) for article in article_list]
                ArticleDataAccess().close_session()
                return output_articles
            else:
                first_article = ArticleDataAccess().get_last_published_article()
                first_output_article = ArticleModel._construct_for_output(first_article, first_article.app_user, first_article.article_group)
                ArticleDataAccess().close_session()
                return first_output_article
        else:
            article = ArticleDataAccess().get_article(article_id, is_admin, user_article_ids)
            output_article = ArticleModel._construct_for_output(article, article.app_user, article.article_group, user_article_ids)
            ArticleDataAccess().close_session()
            return output_article
    
    @requires_authentication
    @requires_role('ADMIN')
    @rest_method
    def post(self, decorator_data=None):
        app_user_id = decorator_data['app_user_id']
        article_data = request.get_json()
        article_data['app_user_id'] = app_user_id
        article = ArticleModel(**article_data)
        result = ArticleDataAccess().create_article(article)
        user = AppUserDataAccess().get_user_by_id(app_user_id)
        article_group = ArticleGroupDataAccess().get_article_group(result.article_group_id) if result.article_group_id is not None else None
        return ArticleModel._construct_for_output(result, user, article_group)
    
    @requires_authentication
    @requires_role('ADMIN')
    @rest_method
    def put(self, article_id, decorator_data=None):
        article_data = request.get_json()
        result = ArticleDataAccess().edit_article(article_id, article_data)
        user = AppUserDataAccess().get_user_by_id(result.app_user_id)
        article_group = ArticleGroupDataAccess().get_article_group(result.article_group_id) if result.article_group_id is not None else None
        return ArticleModel._construct_for_output(result, user, article_group)
    
    @requires_authentication
    @requires_role('ADMIN')
    @rest_method
    def delete(self, article_id, decorator_data=None):
        delete_successful = ArticleDataAccess().delete_article(article_id)

        if delete_successful == True:
            return '', 204
        else:
            return 'Article ID {} not found'.format(article_id), 410