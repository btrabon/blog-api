import datetime
from private_settings import PRIVATE_PAYMENT_PROCESSOR_KEY, APPLICATION_CONFIG, PUBLIC_KEY_FILE, PRIVATE_KEY_FILE

DATABASE_URI = 'postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_URI}/{DB_DATABASE_NAME}'.format(**APPLICATION_CONFIG)

# TODO: generate new keys once we deploy to prod
with open(PUBLIC_KEY_FILE, 'r') as f:
    JWT_PUBLIC_KEY = f.read()
with open(PRIVATE_KEY_FILE, 'r') as f:
    JWT_PRIVATE_KEY = f.read()

JWT_EXPIRATION_TIMESPAN = datetime.timedelta(hours=9)
API_KEY_EXPIRATION_TIMESPAN = datetime.timedelta(days=365)

PAYMENT_PROCESSOR_KEY = PRIVATE_PAYMENT_PROCESSOR_KEY
